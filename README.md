Preview and download YouTube videos from channels that you have "subscribed" to. A script to automate the searching part of YouTube-dl.

I'm also looking for a better name then newget. So if you have any suggestions please let me know.

- [Installation](https://gitlab.com/patrickett/newget#installation)
- [Usage](https://gitlab.com/patrickett/newget#usage)
- [Bugs](https://gitlab.com/patrickett/newget#bugs)
- [Contributing](https://gitlab.com/patrickett/newget#contributing)
- [License](https://gitlab.com/patrickett/newget#license)

# Installation

Check your python version with ```python --version``` if its python 2 then use

    sudo pip3 install newget --upgrade

If its python 3 you use

    sudo pip install newget --upgrade

Run the same command again for updates.

# Git install
Using the proper pip version do

    sudo pip install https://gitlab.com/patrickett/newget/-/archive/master/newget-master.tar

The git install is typically less stable and has experimental features but will get more updates.
# Usage

    newget

Will launch the app if it was installed with pip.

Then it iterates through the feed links in feeds.txt and tries to print them.

    add FEED_URL

Adds the URL of what feed you gave it and restarts to try and pull it.

However this is designed around YouTube. So if you throw any YouTube channel link at it will parse it and add the feed.

    commands
    help
    ?

All three will display the commands. When you reload the commands disappear. However delete does not work in ```old:```

    delete #

Will ask for a confirmation to delete the feed number you selected.

    get FEED_NUMBER/s

This uses YouTube-dl to download whatever feed link's video you pointed it at.

Works with comma separated values ex ```1,7,14``` and will iterate and download them all

    audioget FEED_NUMBER/s

This uses YouTube-dl to download whatever feed link's audio you pointed it at. Tries to get mp3 file format.

Works with comma separated values ex ```1,7,14``` and will iterate and download them all



    mpv FEED_NUMBER/s

This will give the media link to mpv to play. Works very well except it plays at highest resolution and doesn't support multi-link playlists.


    vlc FEED_NUMBER/s

will try to stream the feed to vlc (However this can be buggy with vlc. Expect hiccups).

Also works with comma separated values ex ```1,7,14``` Will add them to a vlc playlist

When vlc closes it will reload the feeds

    stream FEED_NUMBER/s

This will use the ```YouTube-dl -o -``` pipe to vlc. Objectively less buggy than straight vlc streaming. However this does not support playlists. Breaks the pipe every time.


    reload
    r

Redraws the screen and tries to pull the feeds in feeds.txt
Will also work in ```old:```

    exit
    e

Closes the current menu, if in ```new:``` ends the program. If in ```old:``` it returns you to ```new:```

    getall FEED_NUMBER

Downloads all videos on a channel. Like the **WHOLE** thing. **ALL** videos. (You've been warned)
Also it is YouTube specific as of now.

If there are any feed related issues a lot of problems can be solved by editing the ```feeds.txt``` file directly. This is also where it gets the URLs to pull feeds from.
# Windows

I've tried to keep windows users in mind. So most of the commands work on windows as well. However since this is developed on and around Linux there will be issues I don't run into. See the [issues page](https://gitlab.com/patrickett/newget/issues) to report them.

Also to note, I use ***a lot*** of ANSI colors in the terminal to makes things easier to see. On windows, powershell will accept these properly if you [disable legacy console](https://consumingtech.com/enable-disable-legacy-console-for-command-prompt-and-powershell-in-windows-10/) in the settings.
# Bugs

You can submit issues here on the [Gitlab issues page](https://gitlab.com/patrickett/newget/issues).

As far as known bugs. YouTube has this issue between 5-6 Eastern time it will limit the feed reading. It won't actually error out and if left to load it will eventually pull them.

# Contributing

If you would like to contribute or suggest any improvements to the project, please submit a merge request or open a issue with a feature request.


# License
[GNU General Public License, version 3 (GPLv3)](https://www.gnu.org/licenses/gpl.txt)
