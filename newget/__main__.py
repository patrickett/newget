#!/usr/bin/env python3
# encoding: utf-8

###### https://gitlab.com/patrickett/newget ######
# If you have anything you would like to add to the project hit me up on my gitlab. I'm pretty responsive there.
###### https://gitlab.com/patrickett/newget ######

import os
import sys
import re
import time
import argparse
import configparser
import youtube_dl
import vlc
import feedparser
import sys
import traceback
from tld import get_tld
# this is for search functions
from bs4 import BeautifulSoup
import requests
# this allows command history instead of corrupted unicode
import readline
import subprocess

# version string for -v
__version__ = "0.0.9-git"
# menu value for getting a number in the oldget
global_menuvalue = ""


# newget -u to update the rss_feeds. simply newget displays the last parsed feeds also add update command
#


# imports the right version of curses for the os
# get home for windows and nix
# defining clear function based on the operating system
if os.name == 'nt':
    windowsuser = os.getenv('username')
    home = "C:""\\""Users""\\" + windowsuser

    def clear():
        os.system('cls')
else:
    home = os.getenv("HOME")

    def clear():
        os.system('clear')

# defining paths
savepath = '{0}/Videos'.format(home)
ndir = '{0}/.config/newget/'.format(home)
rssfeed = '{0}/.config/newget/feeds.txt'.format(home)
autograb = '{0}/.config/newget/auto.txt'.format(home)
backupfeed = '{0}/.config/newget/backupfeeds.txt'.format(home)
downloaded = '{0}/.config/newget/downloaded.txt'.format(home)

# tries to find home directory
if home == None:
    print("There is no $home. Using install directory as home")
    home = os.path.dirname(os.path.realpath(__file__))
    os.makedirs(ndir, exist_ok=True)
    os.makedirs('{0}/.config/newget/'.format(home), exist_ok=True)

# used for adding youtube channels/ helps with the parsing them into proper feeds
CHANNELID = "https://www.youtube.com/feeds/videos.xml?channel_id="
USERNAME = "https://www.youtube.com/feeds/videos.xml?user="

# colors/readability
CRED = '\033[91m'
CGREEN = '\033[92m'
CEND = '\033[0m'
CYELLOW = '\033[33m'
CCYAN = '\033[36m'
CGREY = '\033[90m'
CBLUE = '\033[94m'


def command_list():
    print("\ncommands:\n"
    +CCYAN+"\nScreen interaction"+CEND+
    "\n[reload] [exit/back]\n"
    +CCYAN+"\nLink interaction"+CEND+
    "\n[get" + CGREEN + " FEED_NUMBER/s" + CEND + "] \n"
    "[mpv" + CGREEN + " FEED_NUMBER/s" + CEND + "] \n"
    "[vlc/stream" + CGREEN + " FEED_NUMBER/s" + CEND + "] \n"
    "[show" + CGREEN + " FEED_NUMBER" + CEND + "] \n"
    +CCYAN+"\nFeed interaction"+CEND+
    "\n[add " + CRED + "URL" + CEND + "] or [delete"  + CGREEN + " FEED_NUMBER" + CEND + "]  \n")
# defines the main program, allows me to catch errors


def main():
    try:
        parser = argparse.ArgumentParser(
            prog='newget', usage="%(prog)s [-h] [-v] [-d] [-a CHANNELID/USERID]")
        group = parser.add_mutually_exclusive_group()
        group.add_argument('--version', '-v', '-V',
                           action='version', version="%(prog)s " + __version__)
        # does not parse the youtube url
        group.add_argument(
            '-a', '--add', help='Adds feed link to feeds.txt Parses if youtube channel.')
        # does nothing
        group.add_argument(
            '-d', '--download', help='Tries to download list of newest videos from auto.txt')
        args = parser.parse_args()

        # creates config dir
        if not os.path.exists(ndir):
            os.makedirs(ndir)
        # sets the feed variable for the feeds and reads the file for the actual feeds
        if os.path.exists(rssfeed):
            rssurls = [line.rstrip('\n') for line in open(rssfeed)]

            # backup feed.txt
            # with open(rssfeed) as f:
            #     with open(backupfeed, "a") as f1:
            #         for line in f:
            #             f1.write(line)

        else:
            print("ERROR: Could not open feeds.txt\n"
                  "Creating a feeds.txt")
            open(rssfeed, 'w')
            print("\nWAIT: Wrote feeds.txt - Restarting App again")
            time.sleep(5)
            main()
        if args.add:
            if "https://www.youtube.com/channel/" in args.add:
                # print("channel")
                args.add = args.add[32:]
                file_path = rssfeed
                with open(file_path, 'a') as file:
                    file.write(CHANNELID + args.add + "\n")
                    print("[+] Youtube channel Feed Added")
                    sys.exit()
            elif "https://www.youtube.com/user/" in args.add:
                # print("user")
                args.add = args.add[29:]
                file_path = rssfeed
                with open(file_path, 'a') as file:
                    file.write(USERNAME + args.add + "\n")
                    print("[+] Youtube user Feed Added")
                    sys.exit()
            else:
                file_path = rssfeed
                with open(file_path, 'a') as file:
                    file.write(args.add + "\n")
                    print("[+] Feed Added")
                    sys.exit()

        else:
            # formatting
            def rss_pull():
                # opens the feeds.txt
                if os.path.exists(rssfeed):
                    rssurls = [line.rstrip('\n') for line in open(rssfeed)]
                else:
                    print("ERROR: Could not open feeds.txt\n"
                          "Creating a feeds.txt")
                    open("feeds.txt", 'w')
                    print("\n[-] Wrote feeds.txt - Restart App again")
                    sys.exit()
                clear()

                # header
                parsing_message_message = CCYAN +"Parsing Feeds" + CEND
                parsing_message_symbol = "[*]"
                print("{:10s}{:^99s}{:>10s}".format(
                    parsing_message_symbol, parsing_message_message,parsing_message_symbol))
                # header

                #layout
                # print("\n{:<10s}{:<10s}{:^65s}{:>15s}{:>10s}".format(
                #     "##", "author", "title", "domain", "date"))
                #layout

                ################# print newest #################
                feeds = []
                count = 1
                # print("{:10s}{:24s}{:30s}{:<30s}".format("[#]","author", "domain", "title"))
                for url in rssurls:
                    # for each url in the feeds.txt it iterates through and creates an output for them see print statment
                    d = feedparser.parse(url)
                    feeds.extend(d)
                    for i in range(1):
                        try:

                            feed = d['entries'][i]
                            res = get_tld(feed.link, as_object=True)

                            # this defines stuff to print
                            feedurl = feed.link
                            feedauthor = CYELLOW + feed.author[:20] + CEND +" "
                            domainname = CRED + "[" + res.domain[:15] + "] " + CEND
                            counter = CGREEN + f"{count:02d}" + CEND
                            feeddate = feed.published[:10]
                            feedtitle = feed.title[:60]

                            # strips the emojis from titles (causes spacing issues)
                            emoji_pattern = re.compile("["
                                                       u"\U0001F600-\U0001F64F"
                                                       u"\U0001F300-\U0001F5FF"
                                                       u"\U0001F680-\U0001F6FF"
                                                       u"\U0001F1E0-\U0001F1FF"
                                                       "]+", flags=re.UNICODE)

                            feedtitle = emoji_pattern.sub(r'', feedtitle)


                            # view 1
                            print("{:<12s}{:<22s}{:>30s}{:60s}{:>13s}".format(
                                 counter, domainname,feedauthor, feedtitle, feeddate))

                            # view 2
                            # print("{:12s}{:30s}{:<60s}{:>25s}{:>10s}".format(
                            #     counter, feedauthor, feedtitle, domainname, feeddate))

                            count += 1
                        except IndexError:
                            print("WARNING: Could not get feed. Check connection.")
                        except:
                            print("ERROR: Issue parsing feed(s)\n"
                                  "Check the feed where it stopped.")
                            sys.exit()
                            # formatting
                parsing_message_message = CCYAN + 'Feeds Parsed.' + CEND
                parsing_message_symbol = "[+]"
                print("{:<10s}{:^99s}{:>10s}".format(
                    parsing_message_symbol, parsing_message_message,parsing_message_symbol))
                #print('\n[+] Feeds Parsed. "?" for help'  "\n")
                # "other commands: [show] [add]")

        def new_menu():
            # first user input
            newmenu = True
            while newmenu == True:
                menu = input("new: ")
                if menu == "all":
                    print("[+] Downloading All.\n"
                          "This can take time...")
                elif menu.startswith("show") == True:
                    try:
                        # cuts the show off the front to leave the number
                        menu = menu[5:]
                        menu = int(menu)
                        # proper menus start at 0 not one this fixes that
                        menu -= 1
                        # finds the value in the rssurls for the right feed
                        menushow = (rssurls[menu])
                        clear()
                        # fixed the number for displaying
                        menu += 1
                        # print("[*] Showing more for " + str(menu) + "\n")
                        showtitle = CCYAN + 'Showing more for '+ CEND + str(menu)
                        more_symbol = "[+]"
                        print("{:<10s}{:^99s}{:>10s}".format(
                            more_symbol, showtitle ,more_symbol))
                        try:
                            feeds = []
                            count = 1
                            # extends the feeds to see the entries
                            feeds.extend(feedparser.parse(menushow).entries)
                            for feed in feeds:
                                # this does nothing/ its never called
                                feedurl = feed.link
                                # gets the domain name for displaying
                                res = get_tld(feed.link, as_object=True)
                                # print (CRED+"[" +res.domain+"] "+CEND+ CGREEN +"["+f"{count:02d}" +"] "+ CEND + feed.title + " - " +CYELLOW+ feed.author+CEND)
                                # the count:02d makes the display numbers more appealing
                                feedurl = feed.link
                                feedauthor = CYELLOW + feed.author[:20] + CEND + " "
                                domainname = CRED + "[" + res.domain + "] " + CEND
                                counter = CGREEN + f"{count:02d}" + CEND
                                feeddate = feed.published[:10]
                                feedtitle = feed.title[:60]

                                # strips the emojis from titles (causes spacing issues)
                                emoji_pattern = re.compile("["
                                                           u"\U0001F600-\U0001F64F"
                                                           u"\U0001F300-\U0001F5FF"
                                                           u"\U0001F680-\U0001F6FF"
                                                           u"\U0001F1E0-\U0001F1FF"
                                                           "]+", flags=re.UNICODE)

                                feedtitle = emoji_pattern.sub(r'', feedtitle)

                                print("{:<22s}{:>30s}{:60s}{:>13s}{:>12s}".format(
                                     domainname,feedauthor, feedtitle, counter,feeddate))
                                count += 1
                            menu -= 1
                            global global_menuvalue
                            global_menuvalue = menu
                        except:
                            print(
                                "ERROR: Failed to get older items in feed" + str(menu))
                        menu += 1
                        showfooter = CCYAN + 'Showing more for '+ CEND + CYELLOW + feed.author + CEND
                        more_symbol = "[+]"
                        print("{:<10s}{:^108s}{:>10s}".format(
                            more_symbol, showfooter ,more_symbol))
                        # print("\n[+] Older items for " + CYELLOW
                        #       + feed.author + CEND + " parsed.\n")
                        old_menu()
                    except KeyboardInterrupt:
                        print(CYELLOW + "\nWARNING:" + CEND +
                              " Shutdown requested. Exiting...")
                        sys.exit()
                    except Exception as ex:
                        print(ex)
                        # print(CYELLOW +"\nWARNING:" +CEND+" Not a valid input")
                elif menu == "?":
                    command_list()
                elif menu == "commands":
                    command_list()
                elif menu == "help":
                    command_list()
                elif menu == "reload":
                    rss_pull()
                elif menu == "r":
                    rss_pull()
                elif menu == "adv":
                    print("\nadvanced commands: [print" + CGREEN + " FEED_NUMBER/s"
                          + CEND + "] [stream" + CGREEN + " FEED_NUMBER/s" + CEND + "]")
                elif menu.startswith("print") == True:
                    if menu.startswith("print") == True:
                        print("works")
                    else:
                        print("fails")
                    # menu = menu[6:]
                    # print(menu)
                elif menu == "example":
                    menu = menu[8:]
                    print("example command")
                elif menu.startswith("vlc") == True:
                    menu = menu[4:]
                    if "," in menu:
                        try:
                            cs_vlc = menu.split(',')
                            intcs_vlc = list(map(int, cs_vlc))
                            multi_vlc = [x - 1 for x in intcs_vlc]
                            len_multi_vlc = len(multi_vlc)
                            vlc_links = []
                            for i in range(len_multi_vlc):
                                multi_vlc_links = multi_vlc[i]
                                getdown = feedparser.parse(
                                    rssurls[multi_vlc_links])
                                # feedparser cant have list for above ^ need loop to run through each item in the list
                                for i in range(1):
                                    feed = getdown['entries'][i]

                                    # print(feed)
                                    for i in range(1):
                                        vlc_links.append(feed.link)

                            vlcvar = ""
                            for i in vlc_links:
                                vlcvar += str(i + " ")
                            # print(vlcvar)
                            os.system("vlc " + "-q " + vlcvar + " vlc://quit")
                        except:
                            print("VLC did not want to work. Multi VLC mode.")
                    else:
                        # single mode
                        menu = int(menu)
                        menu -= 1
                        getdown = feedparser.parse(rssurls[menu])
                        for i in range(1):
                            feed = getdown['entries'][i]
                            # feed.link
                            try:
                                # print(feed.link)
                                os.system("vlc " + "-q " + feed.link)
                                # rss_pull()
                            except:
                                print("VLC did not want to work. Single VLC mode.")
                elif menu.startswith("mpv") == True:
                    menu = menu[4:]
                    if "," in menu:
                        try:
                            cs_vlc = menu.split(',')
                            intcs_vlc = list(map(int, cs_vlc))
                            multi_vlc = [x - 1 for x in intcs_vlc]
                            len_multi_vlc = len(multi_vlc)
                            vlc_links = []
                            for i in range(len_multi_vlc):
                                multi_vlc_links = multi_vlc[i]
                                getdown = feedparser.parse(
                                    rssurls[multi_vlc_links])
                                # feedparser cant have list for above ^ need loop to run through each item in the list
                                for i in range(1):
                                    feed = getdown['entries'][i]

                                    # print(feed)
                                    for i in range(1):
                                        vlc_links.append(feed.link)

                            vlcvar = ""
                            for i in vlc_links:
                                vlcvar += str(i + " ")
                            # print(vlcvar)
                            os.system("mpv " + vlcvar)
                        except:
                            print("VLC did not want to work. Multi VLC mode.")
                    else:
                        # single mode
                        menu = int(menu)
                        menu -= 1
                        getdown = feedparser.parse(rssurls[menu])
                        for i in range(1):
                            feed = getdown['entries'][i]
                            # feed.link
                            try:
                                # print(feed.link)
                                os.system("mpv " + feed.link)
                                # rss_pull()
                            except:
                                print("VLC did not want to work. Single VLC mode.")
                elif menu.startswith("stream") == True:
                    menu = menu[7:]
                    if "," in menu:
                        try:
                            cs_stream = menu.split(',')
                            intcs_stream = list(map(int, cs_stream))
                            multi_stream = [x - 1 for x in intcs_stream]
                            len_multi_stream = len(multi_stream)
                            stream_links = []
                            for i in range(len_multi_stream):
                                multi_stream_links = multi_stream[i]
                                getdown = feedparser.parse(
                                    rssurls[multi_stream_links])
                                # feedparser cant have list for above ^ need loop to run through each item in the list
                                for i in range(1):
                                    feed = getdown['entries'][i]

                                    # print(feed)
                                    for i in range(1):
                                        stream_links.append(feed.link)
                            for i in stream_links:
                                # print (str(i))
                                os.system("youtube-dl -io - "
                                          + str(i) + " " + " | vlc - vlc://quit")
                                # trying to create a playlist does not work, it breaks the pipe everytime

                        except:
                            print("stream did not want to work. Multi Stream mode.")
                    else:
                        # single mode
                        menu = int(menu)
                        menu -= 1
                        getdown = feedparser.parse(rssurls[menu])
                        for i in range(1):
                            feed = getdown['entries'][i]
                            # feed.link
                            try:
                                # print(feed.link)
                                os.system("youtube-dl -io - "
                                          + feed.link + " " + " | vlc -")
                                # rss_pull()
                            except:
                                print(
                                    "stream did not want to work. Single Stream mode.")
                elif menu.startswith("deletelast") == True:
                    readFile = open(rssfeed)
                    lines = readFile.readlines()
                    readFile.close()
                    w = open(rssfeed,'w')
                    w.writelines([item for item in lines[:-1]])
                    w.close()
                    main()
                elif menu.startswith("delete") == True:
                    menu = menu[7:]
                    confirmation = input("Are you sure [y/N]: ")
                    yes = {'yes', 'y', 'ye', 'Y'}
                    no = {'n', 'no', 'No', 'N', ''}
                    if confirmation in yes:
                        try:
                            menu = int(menu)
                            menu -= 1
                            infile = open(rssfeed, 'r').readlines()
                            with open(rssfeed, 'w') as outfile:
                                for index, line in enumerate(infile):
                                    if index != menu:
                                        outfile.write(line)
                        except TypeError:
                            print("Not a proper value. Type Error")
                        except ValueError:
                            print("Not a proper value. Value Error")
                        except:
                            pass
                            # doesnt catch empty user input error


                        menu += 1
                        print(str(menu) + " Deleted. Reload to refresh view")
                    if confirmation in no:
                        print("\nWARNING: Delete not confirmed")
                elif menu.startswith("/yt") == True:

                    clear()
                    youtubetitle = CRED + 'YouTube Search' + CEND
                    searching_symbol = "[?]"
                    print("{:<10s}{:^99s}{:>10s}".format(
                        searching_symbol, youtubetitle,searching_symbol))
                    menu = menu[4:]
                    ytsearch = "https://www.youtube.com/results?search_query="+menu
                    # different agents get different outputs default works fine
                    # header = {'User-Agent': 'Mozilla/5.0'}
                    # r = requests.get(ytsearch, headers=header)
                    r = requests.get(ytsearch)
                    soup = BeautifulSoup(r.text, 'html.parser')
                    yt_result = [ item for item in soup.find_all('div') if item.has_attr('class') and 'yt-lockup-dismissable' in item['class'] ]

                    channel_url_list = []
                    link_count = 1
                    for item in yt_result:
                        try:
                            find_thumbs = item.find_all('img')[0]
                            thumbnail_url = find_thumbs['src'] if not find_thumbs.has_attr('data-thumb') else find_thumbs['data-thumb']

                            videotitle = [x for x in item.find_all('a') if x.has_attr('title')][0]
                            yt_title = videotitle['title']                        #   | video title
                            # print("https://www.youtube.com"+videotitle['href']) #   | video link

                            #  OwO whats this
                            find_chan = item.find_all('div', {'class': 'yt-lockup-byline'})[0]
                            try:
                                chan_link = [x for x in find_chan.find_all('a') if x.has_attr('href') ] [0]
                            except:
                                try:
                                    chan_link = [x for x in find_chan.contents ] [0]
                                except:
                                    print("unknown object")

                                # print("bad elements")
                            # print("https://www.youtube.com"+chan_link['href'])   #  | channel link
                            # print(chan_link.text)                            #  | channel name

                            try:
                                meta_info = [x for x in item.find_all('ul', {'class': 'yt-lockup-meta-info'} ) ] [0]
                                meta_text = meta_info.text
                                info_seperate = meta_text.split('ago')
                                li = info_seperate

                                age = li[0]
                                age = age[:-1]
                                age = re.sub('Streamed ', '', age)

                                views = li[1]
                                views = re.sub('[views]', '', views)
                                # print(meta_info.text) #  | age and view count
                            except:
                                age = "playlist"
                                views = "Playlist"
                            link_count =f"{link_count:02d}"
                            link_count_view = CGREEN+link_count+CEND
                            channel_url_list.append(chan_link['href'])
                            channel_string = chan_link.contents
                            channel_url = ("https://www.youtube.com"+chan_link['href'])

                            channel_name = channel_string[0][:20]+" "
                            channel_name = CYELLOW+channel_name+CEND

                            yt_title = str(yt_title)[:60]

                            emoji_pattern = re.compile("["
                                                      u"\U0001F600-\U0001F64F"  # emoticons
                                                    u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                                                    u"\U0001F680-\U0001F6FF"  # transport & map symbols
                                                    u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                                                    u"\U00002702-\U000027B0"
                                                    u"\U000024C2-\U0001F251"
                                                    u"\U0001f926-\U0001f937"
                                                    u"\u200d"
                                                    u"\u2640-\u2642"
                                                    "]+", flags=re.UNICODE)

                            yt_title = emoji_pattern.sub(r'', yt_title)

                            print("{:<12s}{:>30s}{:63s}{:9s}{:>15s}".format(
                                 link_count_view,channel_name, yt_title,age, views ))

                            # print("{:<12s}{:>30s}{:63s}".format(
                            #      link_count_view,channel_name, yt_title, ))
                            link_count = int(link_count)
                            link_count += 1
                            # BREAKS AT PLAYLIST ITEMS
                        # except IndexError:
                        #     print("Object does not have the right elements. Playlist?")
                        except:
                            pass
                    # print(url_list)
                    youtubefooter = CRED + 'Search Completed' + CEND
                    searching_symbol = "[+]"
                    print("{:<10s}{:^99s}{:>10s}".format(
                    searching_symbol, youtubefooter,searching_symbol))

                    yt_search_menu()
                elif menu.startswith("/tw") == True:
                    clear()
                    twitchtitle = CRED + 'Twitch Search' + CEND
                    searching_symbol = "[?]"
                    print("{:<10s}{:^99s}{:>10s}".format(
                        searching_symbol, twitchtitle,searching_symbol))
                    menu = menu[4:]






                    twitchfooter = CRED + 'Search Completed' + CEND
                    searching_symbol = "[+]"
                    print("{:<10s}{:^99s}{:>10s}".format(
                    searching_symbol, twitchfooter,searching_symbol))

                    tw_search_menu()
                elif menu == "exit":
                    sys.exit()
                elif menu == "e":
                    sys.exit()
                elif menu.startswith("getall") == True:
                    confirmation = input("Are you sure [y/N]: ")
                    yes = {'yes', 'y', 'ye', 'Y'}
                    no = {'n', 'no', 'No', 'N', ''}
                    # need to allow for alternative sites and iterate through them. do a if "yotube" in res.domain :
                    # if this doesnt work it normaally prints just 'href'
                    menu = menu[7:]
                    if confirmation in yes:
                        try:
                            menu = int(menu)
                            menu -= 1
                            getdown = feedparser.parse(rssurls[menu])
                            for i in range(1):
                                feed = getdown['entries'][i]
                            feed_detail = feed.author_detail
                            user_url = feed_detail['href']
                            # print(user_url)
                            try:

                                # youtube-ld options
                                ydl_opts = {
                                    'outtmpl': "{0}/%(uploader)s/%(title)s.%(ext)s".format(savepath),
                                    'writethumbnail': False,
                                    'writedescription': False,
                                    'writeinfojson': False,
                                    'writeannotations': False,
                                    'writesub': False,
                                    'allsubs': False,
                                    'quiet': False
                                }
                                #os.system("youtube-dl "+ feed.link)
                                with youtube_dl.YoutubeDL(ydl_opts) as ydl:
                                    ydl.download([user_url])
                                with open(downloaded, mode='a') as f:
                                    f.write(user_url + '\n')
                            except KeyboardInterrupt:
                                print(CYELLOW + "\nWARNING:" + CEND
                                      + " youtube-dl download Interrupted by user")
                            except:
                                print(CYELLOW + "\nWARNING:" + CEND
                                      + " youtube-dl download error")
                        except Exception as ex:
                            print(CYELLOW + "\nWARNING:" + CEND
                                  + " getall channel download did not work")
                    if confirmation in no:
                        print("Canceled getall command")
                elif menu.startswith('get')  == True:
                    menu = menu[4:]
                    try:
                        if "," in menu:
                            csnewget = menu.split(',')
                            intcsnewget = list(map(int, csnewget))
                            newdown = [x - 1 for x in intcsnewget]
                            # print(newdown)
                            len_newdown = len(newdown)
                            for i in range(0, len_newdown):
                                # print(newdown[i])
                                multiurlgrab = newdown[i]
                                getdown = feedparser.parse(
                                    rssurls[multiurlgrab])
                                # feedparser cant have list for above ^ need loop to run through each item in the list
                                try:
                                    for i in range(1):
                                        feed = getdown['entries'][i]
                                        feedurl = feed.link
                                        res = get_tld(
                                            feed.link, as_object=True)
                                        try:
                                            # youtube-ld options
                                            ydl_opts = {
                                                'outtmpl': "{0}/%(uploader)s/%(title)s.%(ext)s".format(savepath),
                                                'writethumbnail': False,
                                                'writedescription': False,
                                                'writeinfojson': False,
                                                'writeannotations': False,
                                                'writesub': False,
                                                'allsubs': False,
                                                'quiet': False
                                            }
                                            #os.system("youtube-dl "+ feed.link)
                                            with youtube_dl.YoutubeDL(ydl_opts) as ydl:
                                                ydl.download([feed.link])
                                            with open(downloaded, mode='a') as f:
                                                f.write(feed.link + '\n')

                                        except:
                                            print(
                                                CYELLOW + "\nWARNING:" + CEND + " youtube-dl download error")
                                except KeyboardInterrupt:
                                    print(CYELLOW + "\nWARNING:" +
                                          CEND + " Download Interrupted.")
                                except:
                                    print(CYELLOW + "\nWARNING:" + CEND + " Issue collecting video url\n"
                                          "Reload and check your feeds.")

                            print("\n" + CGREEN + "[downloaded]" + CEND + " Finished Downloading " + CGREEN + str(
                                intcsnewget) + CEND + " in " + savepath)

                        elif "," not in menu:
                            # works for single get/ Would prefer comma seperated doing the same thing for each number
                            menu = int(menu)
                            menu -= 1
                            getdown = feedparser.parse(rssurls[menu])
                            # feeds.extend(getdown)
                            try:
                                for i in range(1):
                                    feed = getdown['entries'][i]
                                    feedurl = feed.link
                                    res = get_tld(feed.link, as_object=True)
                                    try:
                                        # youtube-ld options
                                        # youtube-ld options
                                        ydl_opts = {
                                            'outtmpl': "{0}/%(uploader)s/%(title)s.%(ext)s".format(savepath),
                                            'writethumbnail': False,
                                            'writedescription': False,
                                            'writeinfojson': False,
                                            'writeannotations': False,
                                            'writesub': False,
                                            'allsubs': False,
                                            'quiet': False
                                        }
                                        #os.system("youtube-dl "+ feed.link)
                                        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
                                            ydl.download([feed.link])
                                        with open(downloaded, mode='a') as f:
                                            f.write(feed.link + '\n')
                                        menu += 1
                                        print("\n" + CGREEN + "[downloaded]" + CEND + " Finished Downloading " + CGREEN + "[" + str(
                                            menu) + "]" + CEND + " in " + savepath)
                                        #print("\n"+CGREEN+ "["+ str(menu)+"]"+CEND+ " is in "+savepath)
                                    except Exception as ex:
                                        print(ex)
                                        # print(CYELLOW +"\nWARNING:" +CEND+" youtube-dl download error")
                            except:
                                print(CYELLOW + "\nWARNING:" + CEND + " Issue collecting video url\n"
                                      "Reload and check your feeds.")
                    except IndexError:
                        print(CYELLOW + "\nWARNING:"
                              + CEND + " Number out of range")
                    except ValueError:
                        print(CYELLOW + "\nWARNING:"
                              + CEND + " Not a valid input")
                elif menu.startswith("audioget") == True:
                    menu = menu[9:]
                    try:
                        if "," in menu:
                            csnewget = menu.split(',')
                            intcsnewget = list(map(int, csnewget))
                            newdown = [x - 1 for x in intcsnewget]
                            # print(newdown)
                            len_newdown = len(newdown)
                            for i in range(0, len_newdown):
                                # print(newdown[i])
                                multiurlgrab = newdown[i]
                                getdown = feedparser.parse(
                                    rssurls[multiurlgrab])
                                # feedparser cant have list for above ^ need loop to run through each item in the list
                                try:
                                    for i in range(1):
                                        feed = getdown['entries'][i]
                                        feedurl = feed.link
                                        res = get_tld(
                                            feed.link, as_object=True)
                                        try:
                                            # youtube-ld options
                                            ydl_opts = {
                                            'outtmpl': "{0}/%(uploader)s/%(title)s.%(ext)s".format(savepath),
                                            'writethumbnail': False,
                                            'writedescription': False,
                                            'writeinfojson': False,
                                            'writeannotations': False,
                                            'writesub': False,
                                            'allsubs': False,
                                            'quiet': False,
                                            'format': 'bestaudio/best',
                                            'postprocessors': [{
                                            'key': 'FFmpegExtractAudio',
                                            'preferredcodec': 'mp3',
                                            'preferredquality': '192',
                                            }]}


                                            #os.system("youtube-dl "+ feed.link)
                                            with youtube_dl.YoutubeDL(ydl_opts) as ydl:
                                                ydl.download([feed.link])
                                            with open(downloaded, mode='a') as f:
                                                f.write(feed.link + '\n')

                                        except:
                                            print(
                                                CYELLOW + "\nWARNING:" + CEND + " youtube-dl download error")
                                except KeyboardInterrupt:
                                    print(CYELLOW + "\nWARNING:" +
                                          CEND + " Download Interrupted.")
                                except:
                                    print(CYELLOW + "\nWARNING:" + CEND + " Issue collecting video url\n"
                                          "Reload and check your feeds.")

                            print("\n" + CGREEN + "[downloaded]" + CEND + " Finished Downloading " + CGREEN + str(
                                intcsnewget) + CEND + " in " + savepath)

                        elif "," not in menu:
                            # works for single get/ Would prefer comma seperated doing the same thing for each number
                            menu = int(menu)
                            menu -= 1
                            getdown = feedparser.parse(rssurls[menu])
                            # feeds.extend(getdown)
                            try:
                                for i in range(1):
                                    feed = getdown['entries'][i]
                                    feedurl = feed.link
                                    res = get_tld(feed.link, as_object=True)
                                    try:
                                        # youtube-ld options
                                        # youtube-ld options
                                        ydl_opts = {
                                        'outtmpl': "{0}/%(uploader)s/%(title)s.%(ext)s".format(savepath),
                                        'writethumbnail': False,
                                        'writedescription': False,
                                        'writeinfojson': False,
                                        'writeannotations': False,
                                        'writesub': False,
                                        'allsubs': False,
                                        'quiet': False,
                                        'format': 'bestaudio/best',
                                        'postprocessors': [{
                                        'key': 'FFmpegExtractAudio',
                                        'preferredcodec': 'mp3',
                                        'preferredquality': '192',}]}
                                        #os.system("youtube-dl "+ feed.link)
                                        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
                                            ydl.download([feed.link])
                                        with open(downloaded, mode='a') as f:
                                            f.write(feed.link + '\n')
                                        menu += 1
                                        print("\n" + CGREEN + "[downloaded]" + CEND + " Finished Downloading " + CGREEN + "[" + str(
                                            menu) + "]" + CEND + " in " + savepath)
                                        #print("\n"+CGREEN+ "["+ str(menu)+"]"+CEND+ " is in "+savepath)
                                    except Exception as ex:
                                        print(ex)
                                        # print(CYELLOW +"\nWARNING:" +CEND+" youtube-dl download error")
                            except:
                                print(CYELLOW + "\nWARNING:" + CEND + " Issue collecting video url\n"
                                      "Reload and check your feeds.")
                    except IndexError:
                        print(CYELLOW + "\nWARNING:"
                              + CEND + " Number out of range")
                    except ValueError:
                        print(CYELLOW + "\nWARNING:"
                              + CEND + " Not a valid input")
                elif menu.startswith("autoadd") == True:
                    if menu.startswith("print") == True:
                        print("hi")
                    else:
                        print("fail")
                elif menu.startswith("add") == True:
                    menu = menu[4:]
                    if "https://www.youtube.com/channel/" in menu:
                        # print("channel")
                        menu = menu[32:]
                        file_path = rssfeed
                        with open(file_path, 'a') as file:
                            file.write(CHANNELID + menu + "\n")
                            print("[+] Youtube channel Feed Added")
                        main()
                    elif "https://www.youtube.com/user/" in menu:
                        # print("user")
                        menu = menu[29:]
                        file_path = rssfeed
                        with open(file_path, 'a') as file:
                            file.write(USERNAME + menu + "\n")
                            print("[+] Youtube user Feed Added")
                        main()
                    else:
                        file_path = rssfeed
                        with open(file_path, 'a') as file:
                            file.write(menu + "\n")
                            print("[+] Feed Added")
                        main()
                elif menu == "":
                    pass

                else:
                    print(CYELLOW + "\nWARNING:"
                          + CEND + " Not a valid input.")

        def old_menu():
            oldmenu = True
            while oldmenu == True:
                secmenu = input(CCYAN + "old: " + CEND)
                if secmenu == "exit":
                    main()
                elif secmenu == "e":
                    main()
                elif secmenu == "back":
                    main()
                elif "delete" in secmenu:
                    print("Cannot delete in old:")
                elif secmenu == "?":
                    command_list()
                elif secmenu == "commands":
                    command_list()
                elif secmenu == "help":
                    command_list()
                elif secmenu == "":
                    continue
                elif secmenu == 'reload':
                    # print(global_menuvalue)
                    menushow = (rssurls[global_menuvalue])
                    clear()
                    menvalue = global_menuvalue
                    menvalue += 1
                    # fixed the number for displaying
                    try:
                        feeds = []
                        count = 1
                        # extends the feeds to see the entries
                        feeds.extend(feedparser.parse(menushow).entries)
                        print("[*] Showing more for " + str(menvalue) + "\n")
                        for feed in feeds:
                                # this does nothing/ its never called
                            feedurl = feed.link
                            # gets the domain name for displaying
                            res = get_tld(feed.link, as_object=True)
                            # print (CRED+"[" +res.domain+"] "+CEND+ CGREEN +"["+f"{count:02d}" +"] "+ CEND + feed.title + " - " +CYELLOW+ feed.author+CEND)
                            # the count:02d makes the display numbers more appealing
                            print(CGREEN + "[" + f"{count:02d}" + "] " + CEND + CRED + "[" + res.domain +
                                  "] " + CCYAN + feed.published[:10] + CEND + " - " + feed.title[:70])
                            count += 1
                    except:
                        print("ERROR: Failed to get older items in feed" + "menu#")
                    print("\n[+] Older items for " + CYELLOW
                          + feed.author + CEND + " parsed.\n")
                elif secmenu == 'r':
                    # print(global_menuvalue)
                    menushow = (rssurls[global_menuvalue])
                    clear()
                    menvalue = global_menuvalue
                    menvalue += 1
                    # fixed the number for displaying
                    try:
                        feeds = []
                        count = 1
                        # extends the feeds to see the entries
                        feeds.extend(feedparser.parse(menushow).entries)
                        print("[*] Showing more for " + str(menvalue) + "\n")
                        for feed in feeds:
                            # this does nothing/ its never called
                            feedurl = feed.link
                            # gets the domain name for displaying
                            res = get_tld(feed.link, as_object=True)
                            # print (CRED+"[" +res.domain+"] "+CEND+ CGREEN +"["+f"{count:02d}" +"] "+ CEND + feed.title + " - " +CYELLOW+ feed.author+CEND)
                            # the count:02d makes the display numbers more appealing
                            print(CGREEN + "[" + f"{count:02d}" + "] " + CEND + CRED + "[" + res.domain +
                                  "] " + CCYAN + feed.published[:10] + CEND + " - " + feed.title[:70])
                            count += 1
                    except:
                        print("ERROR: Failed to get older items in feed" + "menu#")
                    print("\n[+] Older items for " + CYELLOW
                          + feed.author + CEND + " parsed.\n")
                elif secmenu.startswith("vlc") == True:
                    secmenu = secmenu[4:]
                    try:
                        if "," in secmenu:
                            #####################################################
                            menushow1 = (rssurls[global_menuvalue])
                            # print(secmenu)
                            # print("has comma")
                            ###
                            csvlc = secmenu.split(',')
                            intcsvlc = list(map(int, csvlc))
                            olddown = [x - 1 for x in intcsvlc]
                            # print(olddown)
                            # This gets the numbers for the grabbing ex >get 1,2 | [0, 1]
                            len_olddown = len(olddown)
                            vlc_links = []
                            for i in range(0, len_olddown):
                                    # print(olddown[i])
                                    #  ^ just the two numbers on seperate loops
                                multiurlgrab = olddown[i]
                                # print(multiurlgrab)
                                oldgetdown = feedparser.parse(menushow1)
                                # logic for getting secmenu mutliple items
                                # feedparser cant have list for above ^ need loop to run through each item in the list
                                try:
                                        # print(oldgetdown)
                                    for i in range(1):
                                        try:
                                            feeds = []
                                            feeds.extend(
                                                feedparser.parse(menushow1).entries)
                                            secget = (feeds[multiurlgrab])
                                        except:
                                            print("Issue getting old feed")
                                except:
                                    print(ex)
                                vlc_links.append(secget.link)
                            vlcvar = ""
                            for i in vlc_links:
                                vlcvar += str(i + " ")
                            # print(vlcvar)
                            # os.system("vlc " + "-q "+ vlcvar +" vlc://quit")
                            os.system("vlc " + "-q " + vlcvar)

                        else:
                            # single mode
                            intsecmenu = int(secmenu)
                            intsecmenu -= 1
                            # current urls on the screen
                            menushow1 = (rssurls[global_menuvalue])
                            # print("has no comma")
                            try:
                                feeds = []
                                feeds.extend(
                                    feedparser.parse(menushow1).entries)
                                secget = (feeds[intsecmenu])

                            except:
                                print(
                                    "VLC did not want to work. Single Stream mode.")
                            # print(secget)
                            os.system("vlc " + "-q " + secget.link)
                            # os.system("vlc " + "-q "+ secget.link +" vlc://quit")

                    except IndexError:
                        print(CYELLOW + "\nWARNING:"
                              + CEND + " Number out of range")
                    except ValueError:
                        print(CYELLOW + "\nWARNING:"
                              + CEND + " Not a valid value")
                    except Exception as ex:
                        print(ex)
                elif secmenu.startswith("mpv") == True:
                    secmenu = secmenu[4:]
                    try:
                        if "," in secmenu:
                            #####################################################
                            menushow1 = (rssurls[global_menuvalue])
                            # print(secmenu)
                            # print("has comma")
                            ###
                            csvlc = secmenu.split(',')
                            intcsvlc = list(map(int, csvlc))
                            olddown = [x - 1 for x in intcsvlc]
                            # print(olddown)
                            # This gets the numbers for the grabbing ex >get 1,2 | [0, 1]
                            len_olddown = len(olddown)
                            vlc_links = []
                            for i in range(0, len_olddown):
                                # print(olddown[i])
                                #  ^ just the two numbers on seperate loops
                                multiurlgrab = olddown[i]
                                # print(multiurlgrab)
                                oldgetdown = feedparser.parse(menushow1)
                                # logic for getting secmenu mutliple items
                                # feedparser cant have list for above ^ need loop to run through each item in the list
                                try:
                                    # print(oldgetdown)
                                    for i in range(1):
                                        try:
                                            feeds = []
                                            feeds.extend(
                                                feedparser.parse(menushow1).entries)
                                            secget = (feeds[multiurlgrab])
                                        except:
                                            print("Issue getting old feed")
                                except:
                                    print(ex)
                                vlc_links.append(secget.link)
                            vlcvar = ""
                            for i in vlc_links:
                                vlcvar += str(i + " ")
                            # print(vlcvar)
                            # os.system("vlc " + "-q "+ vlcvar +" vlc://quit")
                            os.system("mpv " + vlcvar)

                        else:
                            # single mode
                            intsecmenu = int(secmenu)
                            intsecmenu -= 1
                            # current urls on the screen
                            menushow1 = (rssurls[global_menuvalue])
                            # print("has no comma")
                            try:
                                feeds = []
                                feeds.extend(
                                    feedparser.parse(menushow1).entries)
                                secget = (feeds[intsecmenu])

                            except:
                                print(
                                    "VLC did not want to work. Single Stream mode.")
                            # print(secget)
                            os.system("mpv " + secget.link)
                            # os.system("vlc " + "-q "+ secget.link +" vlc://quit")

                    except IndexError:
                        print(CYELLOW + "\nWARNING:"
                              + CEND + " Number out of range")
                    except ValueError:
                        print(CYELLOW + "\nWARNING:"
                              + CEND + " Not a valid value")
                    except Exception as ex:
                        print(ex)
                elif secmenu.startswith("stream") == True:
                    secmenu = secmenu[7:]
                    try:
                        if "," in secmenu:
                            #####################################################
                            menushow1 = (rssurls[global_menuvalue])
                            # print(secmenu)
                            # print("has comma")
                            ###
                            csstream = secmenu.split(',')
                            intcsstream = list(map(int, csstream))
                            olddown = [x - 1 for x in intcsstream]
                            # print(olddown)
                            # This gets the numbers for the grabbing ex >get 1,2 | [0, 1]
                            len_olddown = len(olddown)
                            vlc_links = []
                            for i in range(0, len_olddown):
                                # print(olddown[i])
                                #  ^ just the two numbers on seperate loops
                                multiurlgrab = olddown[i]
                                # print(multiurlgrab)
                                oldgetdown = feedparser.parse(menushow1)
                                # logic for getting secmenu mutliple items
                                # feedparser cant have list for above ^ need loop to run through each item in the list
                                try:
                                    # print(oldgetdown)
                                    for i in range(1):
                                        try:
                                            feeds = []
                                            feeds.extend(
                                                feedparser.parse(menushow1).entries)
                                            secget = (feeds[multiurlgrab])
                                        except:
                                            print("Issue getting old feed")
                                except:
                                    print(ex)
                                vlc_links.append(secget.link)
                            vlcvar = ""
                            for i in vlc_links:
                                vlcvar += str(i + " ")
                            # print(vlcvar)
                            # os.system("vlc " + "-q "+ vlcvar +" vlc://quit")
                            for i in vlc_links:
                                # print (str(i))
                                os.system("youtube-dl -io - "
                                          + str(i) + " " + " | vlc - ")
                                # trying to create a playlist does not work, it breaks the pipe everytime

                        else:
                            # single mode
                            intsecmenu = int(secmenu)
                            intsecmenu -= 1
                            # current urls on the screen
                            menushow1 = (rssurls[global_menuvalue])
                            # print("has no comma")
                            try:
                                feeds = []
                                feeds.extend(
                                    feedparser.parse(menushow1).entries)
                                secget = (feeds[intsecmenu])

                            except:
                                print(
                                    "VLC did not want to work. Single Stream mode.")
                            # print(secget)
                            # os.system("vlc " + "-q "+ secget.link +" vlc://quit")
                                # print (str(i))
                            os.system("youtube-dl -io - "
                                      + secget.link + " " + " | vlc -")
                            # trying to create a playlist does not work, it breaks the pipe everytime

                    except IndexError:
                        print(CYELLOW + "\nWARNING:"
                              + CEND + " Number out of range")
                    except ValueError:
                        print(CYELLOW + "\nWARNING:"
                              + CEND + " Not a valid value")
                    except Exception as ex:
                        print(ex)
                elif secmenu.startswith("get") == True:
                    secmenu = secmenu[4:]
                    try:
                        if "," in secmenu:
                            #####################################################
                            menushow1 = (rssurls[global_menuvalue])
                            # print(secmenu)
                            # print("has comma")
                            ###
                            csoldget = secmenu.split(',')
                            intcsoldget = list(map(int, csoldget))
                            olddown = [x - 1 for x in intcsoldget]
                            # print(olddown)
                            # This gets the numbers for the grabbing ex >get 1,2 | [0, 1]
                            len_olddown = len(olddown)
                            for i in range(0, len_olddown):
                                # print(olddown[i])
                                #  ^ just the two numbers on seperate loops
                                multiurlgrab = olddown[i]
                                # print(multiurlgrab)
                                oldgetdown = feedparser.parse(menushow1)
                                # logic for getting secmenu mutliple items
                                # feedparser cant have list for above ^ need loop to run through each item in the list
                                try:
                                    # print(oldgetdown)
                                    for i in range(1):
                                        try:
                                            feeds = []
                                            feeds.extend(
                                                feedparser.parse(menushow1).entries)
                                            secget = (feeds[multiurlgrab])
                                            # print(secget.link)
                                            #os.system("youtube-dl "+secget.link)
                                            # youtube-ld options
                                            ydl_opts = {
                                                'outtmpl': "{0}/%(uploader)s/%(title)s.%(ext)s".format(savepath),
                                                'writethumbnail': False,
                                                'writedescription': False,
                                                'writeinfojson': False,
                                                'writeannotations': False,
                                                'writesub': False,
                                                'allsubs': False,
                                                'quiet': False
                                            }
                                            #os.system("youtube-dl "+ feed.link)
                                            with youtube_dl.YoutubeDL(ydl_opts) as ydl:
                                                ydl.download([secget.link])
                                            with open(downloaded, mode='a') as f:
                                                f.write(secget.link + '\n')

                                        except:
                                            print(GREENOW + "\nWARNING:"
                                                  + CEND + " Youtube-DL issue")
                                except KeyboardInterrupt:
                                    print(CYELLOW + "\nWARNING:" +
                                          CEND + " Download Interrupted.")
                                except:
                                    print(CYELLOW + "\nWARNING:" + CEND + " Issue collecting video url\n"
                                          "Reload and check your feeds.")

                            print("\n" + CGREEN + "[downloaded]" + CEND + " Finished Downloading " + CGREEN + str(
                                intcsoldget) + CEND + " in " + savepath)

                        elif "," not in secmenu:
                            intsecmenu = int(secmenu)
                            intsecmenu -= 1
                            # current urls on the screen
                            menushow1 = (rssurls[global_menuvalue])
                            # print("has no comma")
                            try:
                                feeds = []
                                feeds.extend(
                                    feedparser.parse(menushow1).entries)
                                secget = (feeds[intsecmenu])
                                # print(secget.link)
                                #os.system("youtube-dl "+secget.link)
                                # youtube-ld options
                                ydl_opts = {
                                    'outtmpl': "{0}/%(uploader)s/%(title)s.%(ext)s".format(savepath),
                                    'writethumbnail': False,
                                    'writedescription': False,
                                    'writeinfojson': False,
                                    'writeannotations': False,
                                    'writesub': False,
                                    'allsubs': False,
                                    'quiet': False
                                }
                                #os.system("youtube-dl "+ feed.link)
                                with youtube_dl.YoutubeDL(ydl_opts) as ydl:
                                    ydl.download([secget.link])
                                with open(downloaded, mode='a') as f:
                                    f.write(secget.link + '\n')
                            except KeyboardInterrupt:
                                print(CYELLOW + "\nWARNING:" + CEND
                                      + " youtube-dl download canceled")
                            except:
                                print(CYELLOW + "\nWARNING:" + CEND +
                                      " Issue collecting video url. Reload and check your feeds.")

                    except IndexError:
                        print(CYELLOW + "\nWARNING:"
                              + CEND + " Number out of range")
                    except ValueError:
                        print(CYELLOW + "\nWARNING:"
                              + CEND + " Not a valid value")
                    # secondfeed = feedparser.parse(secmenuget)
                else:
                    print(CYELLOW + "\nWARNING:"
                          + CEND + " Not a valid input.")
        def yt_search_menu():
            ytsearchmenu = True
            while ytsearchmenu == True:
                ytsearch = input("yt: ")
                if ytsearch == "":
                    pass
                elif "add" in ytsearch:
                    print("has add")
                elif ytsearch == "e":
                    main()
                elif "delete" in ytsearch:
                    print("Delete only works in the main menu.")
                elif ytsearch == "?":
                    command_list()
                elif ytsearch == "help":
                    command_list()
                else:
                    print("Not a valid input")
        def tw_search_menu():
            ytsearchmenu = True
            while ytsearchmenu == True:
                ytsearch = input("tw: ")
                if ytsearch == "":
                    pass
                elif "add" in ytsearch:
                    print("has add")
                elif ytsearch == "e":
                    main()
                elif "delete" in ytsearch:
                    print("Delete only works in the main menu.")
                elif ytsearch == "?":
                    command_list()
                elif ytsearch == "help":
                    command_list()
                else:
                    print("Not a valid input")
        # pulls new Feeds and starts the main menu
        rss_pull()
        new_menu()
    except KeyboardInterrupt:
        print(CYELLOW + "\nWARNING:" + CEND +
              " Shutdown requested. Exiting...")
        sys.exit()
    except Exception as ex:
        print(ex)
        traceback.print_exc(file=sys.stdout)
        sys.exit(0)


if __name__ == "__main__":
    main()
