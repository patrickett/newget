#!/usr/bin/env python
# coding: utf-8
#coding

from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name='newget',
    packages=find_packages(),
    description='A script to automate the searching part of youtube-dl',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://gitlab.com/patrickett/newget',
    download_url='https://gitlab.com/patrickett/newget/-/archive/master/newget-master.tar',
    keywords=['newget', 'rss', 'youtube', 'youtube_dl'],
    install_requires=['youtube-dl','feedparser','tld','vlc','readline','bs4'],
    version='0.0.9-git',
    entry_points={
          'console_scripts': [
              'newget = newget.__main__:main'
          ]
      },
      )
