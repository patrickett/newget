# CHANGELOG

patrickett - ```6e1d9059bbe12e20d29ddbf095f0812466e77af1```

1/17/2019
- added ```audioget``` command to only get mp3 files from a given link
- flushed out more of the YouTube search function
- added twitch search menu
- reorganized commands when ```?``` is used (looks nicer)
- changed how menu commands work. Now uses ```x.startswith(y)```
- changed deleteprev to deletelast as a viable command



patrickett - ```4b361b662d134f726ef66568ef7634de0b3a2238```
- made uniform formatting
- fixed emojis in old menu
- fixed naming convention for menus and functions
- fixed ENTER being pressed as an error
- added YouTube search core. Pulls elements in specified loops. Needs single loop and menu commands.
